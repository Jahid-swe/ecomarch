from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('blog',views.blog, name='blog'),
    path('cart',views.cart, name='cart'),
    path('product',views.product, name='product'),
    path('shop',views.shop, name='shop'),
    path('contact',views.contact, name='contact'),
    path('regular',views.regular, name='regular'),
    path('registration',views.registration,name='registration'),


] 