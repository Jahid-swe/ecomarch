from django.db import models



class contract_del(models.Model):
    phone_number = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    address = models.CharField(max_length=80)


class registration1(models.Model):
    # email = models.CharField(max_length=30)
    # psw =  models.CharField(max_length=30)
    # psw_repeat = models.CharField(max_length=30)
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=30)

    def __str__(self):
        return self.username

class shop_product_adding(models.Model):
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/')
    product_name = models.CharField(max_length=50)
    product_price = models.CharField(max_length=6)

    def __str__(self):
        return self.product_name
        


class blog_post_details(models.Model):
    blog_photo = models.ImageField(upload_to='photos/%Y/%m/%d/')
    blog_text = models.CharField(max_length=500)


    def __str__(self):
        return self.blog_text
