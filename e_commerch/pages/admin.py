from django.contrib import admin

# Register your models here.
from .models import contract_del,registration1,shop_product_adding,blog_post_details

admin.site.register(contract_del),
admin.site.register(registration1),
admin.site.register(shop_product_adding),
admin.site.register(blog_post_details),