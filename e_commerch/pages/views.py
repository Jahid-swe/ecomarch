from django.conf import settings
from django.core.mail import send_mail

from django.contrib import messages
from django.contrib.auth.models import User, auth
from .models import registration1,shop_product_adding,contract_del,blog_post_details
from django.shortcuts import render,redirect


def index(request):
    return render(request,'pages/index.html')


def about(request):
    return render(request,'pages/about.html')


def blog(request):
    get_blog_post_details = blog_post_details.objects.all()

    context = {
         'get_blog_post_details' : get_blog_post_details,
    }
    return render(request,'pages/blog.html',context)


def cart(request):
    return render(request ,'pages/cart.html')


def product(request):
    return render(request,'pages/product.html')

def shop(request):
    get_shop_product_adding = shop_product_adding.objects.all()

    context = {
        'get_shop_product_adding' : get_shop_product_adding,
    }
    
    return render(request,'pages/shop.html',context)

def contact(request):
    if request.method == 'POST':
        name  = request.POST['name']
        email = request.POST['email']
        phone = request.POST['phone']
        message = request.POST['message']

        send_mail('Contact Form',
         message,
         settings.EMAIL_HOST_USER,
         ['programmerjahid162@gmail.com'],
         fail_silently=False,
         )
    
    get_contract_del = contract_del.objects.all()

    context ={
        'get_contract_del' : get_contract_del,
    } 
    return render(request,'pages/contact.html',context)

def regular(request):
    return render(request,'pages/regular.html')


def registration(request):
    if request.method == 'POST':
        username = request.POST['username']
        password =request.POST['password']

        user = registration1(username=username,password=password)
        user.save()
        print("works")
        return redirect('/')
    
    else:

        return render(request,'pages/registration.html')

